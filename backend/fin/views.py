from typing import Any
from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.views import APIView

from fin.models import financeHistory
from fin.serealizer import financeSerealizer
# Create your views here.


# class Financeview(ModelViewSet):
#     def id(self, request, *args, **kwargs):
#         id_user = request.query_params.get('id_user')
#         return id_user
#     queryset = financeHistory.objects.filter(id_account=id())
#     serializer_class = financeSerealizer

class Financeview(ModelViewSet):
    serializer_class = financeSerealizer

    def get(self, request, id_user=None):
        queryset = financeHistory.objects.filter(id_account=id_user)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)