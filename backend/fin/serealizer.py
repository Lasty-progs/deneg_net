from rest_framework.serializers import ModelSerializer
from fin.models import financeHistory


class financeSerealizer(ModelSerializer):
    class Meta():
        model = financeHistory
        fields = ['product', 'hygiene', 'health', 'fastfood', 'internet', 'transport', 'other']

# rewrite 