from django.db import models

# Create your models here.
class financeHistory(models.Model):
    id_account = models.IntegerField(default=0)
    product = models.IntegerField(default=0)
    hygiene = models.IntegerField(default=0)
    health = models.IntegerField(default=0)
    fastfood = models.IntegerField(default=0)
    internet = models.IntegerField(default=0)
    transport = models.IntegerField(default=0)
    other = models.IntegerField(default=0)
