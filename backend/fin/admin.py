from django.contrib import admin

from .models import financeHistory

# Register your models here.
admin.site.register(financeHistory)
