from rest_framework.serializers import ModelSerializer
from registration.models import account


class AccountsSerealizer(ModelSerializer):
    class Meta():
        model = account
        fields = ['login']