FROM python:3
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
WORKDIR /app
ADD . /app
EXPOSE 8000
RUN python -m pip install --upgrade pip && pip install -r req.txt
